FROM node:18-alpine3.14
RUN apk update && apk upgrade && sync
WORKDIR /app/temperato
COPY . /app/temperato
RUN npm ci
CMD ["npm", "start"]