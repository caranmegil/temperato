/*
 temperato.ts - Driver of querying service
 Copyright (C) 2021,2022  William R. Moore <william@nerderium.com>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import 'dotenv/config';
import twilio from 'twilio';
import fetch from 'node-fetch';
import { Gotify, gotify } from 'gotify';

const PORT = parseInt(process.env.PORT) || 3000;
const SET_POINT = parseFloat(process.env.SET_POINT);
const MESSAGE = process.env.MESSAGE;
const EARLY_WARNING = parseFloat(process.env.EARLY_WARNING ?? process.env.SET_POINT);
const EARLY_WARNING_MESSAGE = process.env.EARLY_WARNING_MESSAGE;
const ENDPOINT = process.env.ENDPOINT;
const GOTIFY_HOST = process.env.GOTIFY_HOST;
const GOTIFY_TOKEN = process.env.GOTIFY_TOKEN;
const TICK_THRESHOLD = parseInt(process.env.TICK_THRESHOLD ?? '15');	
const SLEEP_TIMER = 1000;

let numberOfTicks = 0;
let numberOfEarlyTicks = 0;

async function temperatoTick() {
  try {
    const response = await fetch(ENDPOINT);
    const data: any = await response.json();
    
    if (data.temperature < SET_POINT && data.temperature >= EARLY_WARNING) {
      numberOfEarlyTicks++;
      if (numberOfEarlyTicks == TICK_THRESHOLD) {
        numberOfEarlyTicks = 0;
      }

      if (numberOfEarlyTicks == 0) {
        await gotify({
          server: GOTIFY_HOST,
          app: GOTIFY_TOKEN,
          title: 'Warning!',
          message: EARLY_WARNING_MESSAGE,
          priority: 10,
        });
      }
    }
    if (data.temperature >= SET_POINT) {
      numberOfTicks++;
      if (numberOfTicks == TICK_THRESHOLD) {
        numberOfTicks = 0;
      }

      if (numberOfTicks == 0) {
        await gotify({
          server: GOTIFY_HOST,
          app: GOTIFY_TOKEN,
          title: 'Alert!',
          message: MESSAGE,
          priority: 10,
        });
      }
    }
  } catch (e) {
    console.error(new Date(), e);
  }
}

setInterval(() => temperatoTick(), SLEEP_TIMER);
